/* This product customizer was created from scratch and attached to this theme
Author: Philip Diaz at make8.digital
Date Created: 21/05/2021 */
(function($,m8,d,ri,rin,di) {
    shv = true;
    $(d).on("click",".custom-product-option-div", function() {
        var optiondiv = d.querySelectorAll(".custom-product-option-div"),
        inputcheck = d.querySelector(".customizer__words-input"),
        preview = d.querySelector("[js_custom_font_text_value]");
        for(i=0;i<optiondiv.length;i++) {
           if(optiondiv[i].classList.contains("active")){
               optiondiv[i].classList.remove("active");
               for(x=0;x<optiondiv[i].children.length;x++) {
                   if(x>0) {
                    if(optiondiv[i].children[x].classList.contains("note-engraving-position") == false) {
                       optiondiv[i].children[x].classList.add("d-none");
                      }
                   }
               }
           };
        }
        this.classList.add("active");
        for(x=0;x<this.children.length;x++) {
            if(x>0) {
                   this.children[x].classList.remove("d-none");
            }
        }   
        if(this.getAttribute("data-current-div") === "custom-order-input") {
            if(inputcheck.checked) {       
            preview.classList.remove("d-none");
            }
            if(this.getAttribute("data-value") === "Acacia") {
                preview.classList.remove("custom-font-text-value2");    
                preview.classList.add("custom-font-text-value");
            } 
            if(this.getAttribute("data-value") === "Maple") {
                preview.classList.add("custom-font-text-value2");    
                preview.classList.remove("custom-font-text-value");
            } 
        } else {
            if(inputcheck.checked) {
            preview.classList.add("d-none");
            }
        }
    });
    $(d).on("click",".custom-product-option-word-choice", function() {
        var activeinput = d.querySelectorAll(".custom-product-option-word-input-choice");
        var activebtn = d.querySelectorAll(".custom-product-option-word-choice");
        var activelabel = d.querySelector(".custom_words_variant_mr");
        var activeprice = d.querySelector(".font-family-time-new-roman");
        var jscustomfirstwordrender = d.querySelector(".js-custom-first-word-render");
        var jscustomsecondwordrender = d.querySelector(".js-custom-second-word-render");
        var jscustomthirdwordrender = d.querySelector(".js-custom-third-word-render");
        var jscustomfirstwordrenderdiv = d.querySelector(".js-custom-first-word-render-div");
        var jscustomsecondwordrenderdiv = d.querySelector(".js-custom-second-word-render-div");
        var jscustomthirdwordrenderdiv = d.querySelector(".js-custom-third-word-render-div");
        var jscustomfirstword = d.querySelector(".js-custom-first-word");
        var jscustomsecondword = d.querySelector(".js-custom-second-word");
        var jscustomthirdword = d.querySelector(".js-custom-third-word");
        var activevalue = this.innerHTML;
        [].forEach.call(activebtn, function(el) {
            el.classList.remove("active");
        })
        var inputshow = 0;
        if(activevalue.includes("One") || activevalue.includes("one")) {
            inputshow = 0;
            activelabel.textContent = this.firstChild.textContent;
            activeprice.textContent = this.children[2].textContent;    
            jscustomfirstwordrender.textContent = "";  
            jscustomfirstwordrenderdiv.textContent = "";    
            jscustomsecondwordrender.textContent = ""; 
            jscustomsecondwordrenderdiv.textContent = ""; 
            jscustomthirdwordrender.textContent = ""; 
            jscustomthirdwordrenderdiv.textContent = "";  
            jscustomfirstword.value = "";
            jscustomsecondword.value = "";
            jscustomthirdword.value = "";
        }
        if(activevalue.includes("Two") || activevalue.includes("two")) {
            inputshow = 1;
            activelabel.textContent = this.firstChild.textContent;
            activeprice.textContent = this.children[2].textContent;
            jscustomfirstwordrender.textContent = "";  
            jscustomfirstwordrenderdiv.textContent = "";    
            jscustomsecondwordrender.textContent = ""; 
            jscustomsecondwordrenderdiv.textContent = ""; 
            jscustomthirdwordrender.textContent = ""; 
            jscustomthirdwordrenderdiv.textContent = ""; 
            jscustomfirstword.value = "";
            jscustomsecondword.value = "";
            jscustomthirdword.value = "";
        }        
        if(activevalue.includes("Three") || activevalue.includes("three")) {
            inputshow = 2;
            activelabel.textContent = this.firstChild.textContent;
            activeprice.textContent = this.children[2].textContent;
            jscustomfirstwordrender.textContent = "";  
            jscustomfirstwordrenderdiv.textContent = "";    
            jscustomsecondwordrender.textContent = ""; 
            jscustomsecondwordrenderdiv.textContent = ""; 
            jscustomthirdwordrender.textContent = ""; 
            jscustomthirdwordrenderdiv.textContent = ""; 
            jscustomfirstword.value = "";
            jscustomsecondword.value = "";
            jscustomthirdword.value = "";
        }      
        switch(inputshow) {
            case 0:
            activeinput[0].classList.remove("d-none");
            activeinput[0].classList.remove("custom-product-two-first-input");
            activeinput[0].classList.remove("custom-product-three-first-input");
            activeinput[0].classList.add("custom-product-one-input");
            activeinput[1].classList.remove("custom-product-two-second-input");
            activeinput[1].classList.remove("custom-product-three-first-input");
            activeinput[1].classList.add("d-none");
            activeinput[1].classList.remove("custom-product-three-third-input");
            activeinput[2].classList.add("d-none");
            this.classList.add("active");     
            break;
            case 1:            
            activeinput[0].classList.remove("d-none");
            activeinput[0].classList.remove("custom-product-one-input");
            activeinput[0].classList.add("custom-product-two-first-input");
            activeinput[0].classList.remove("custom-product-three-first-input");
            activeinput[1].classList.remove("d-none");
            activeinput[1].classList.add("custom-product-two-second-input");
            activeinput[1].classList.remove("custom-product-three-second-input");
            activeinput[2].classList.add("d-none");
            activeinput[2].classList.remove("custom-product-three-third-input");
            this.classList.add("active");  
            break;
            case 2:
            activeinput[0].classList.remove("custom-product-one-input");   
            activeinput[0].classList.remove("custom-product-two-first-input");
            activeinput[0].classList.add("custom-product-three-first-input");
            activeinput[0].classList.remove("d-none");
            activeinput[1].classList.remove("custom-product-two-second-input");
            activeinput[1].classList.add("custom-product-three-second-input");
            activeinput[1].classList.remove("d-none");
            activeinput[2].classList.add("custom-product-three-third-input");
            activeinput[2].classList.remove("d-none");
            this.classList.add("active");  
            break;
            default:
          }
    });
    $(d).on("click",".custom-select-wrapper", function() {
        this.querySelector('.custom-select').classList.toggle('open');
    })
    $(d).on("click",".custom-option", function() {
        var options = d.querySelectorAll(".custom-option");
        var select = d.querySelector(".custom-product-option-fonts-selected");
        var selecttrigger = d.querySelector(".custom-select__trigger");
        var jscustomfirstwordrender = d.querySelector(".js-custom-first-word-render");
        var jscustomsecondwordrender = d.querySelector(".js-custom-second-word-render");
        var jscustomthirdwordrender = d.querySelector(".js-custom-third-word-render");
        var jscustomfirstwordrenderdiv = d.querySelector(".js-custom-first-word-render-div");
        var jscustomsecondwordrenderdiv = d.querySelector(".js-custom-second-word-render-div");
        var jscustomthirdwordrenderdiv = d.querySelector(".js-custom-third-word-render-div");
        [].forEach.call(options,function(el){
            if(el.classList.contains("selected")){
                el.classList.remove("selected");
            }      
        });   
        for(i=1;i<4;i++) {
            if(selecttrigger.firstChild.classList.contains("custom-option-font" + i)) {
                selecttrigger.firstChild.classList.remove("custom-option-font" + i);
            }
        };    
        for(i=1;i<4;i++) {
            if(jscustomfirstwordrender.classList.contains("custom-option-font" + i)) {
                jscustomfirstwordrender.classList.remove("custom-option-font" + i);
            }
            if(jscustomfirstwordrenderdiv.classList.contains("custom-option-font" + i)) {
                jscustomfirstwordrenderdiv.classList.remove("custom-option-font" + i);
            }
            if(jscustomsecondwordrender.classList.contains("custom-option-font" + i)) {
                jscustomsecondwordrender.classList.remove("custom-option-font" + i);
            }
            if(jscustomsecondwordrenderdiv.classList.contains("custom-option-font" + i)) {
                jscustomsecondwordrenderdiv.classList.remove("custom-option-font" + i);
            }
            if(jscustomthirdwordrender.classList.contains("custom-option-font" + i)) {
                jscustomthirdwordrender.classList.remove("custom-option-font" + i);
            }
            if(jscustomthirdwordrenderdiv.classList.contains("custom-option-font" + i)) {
                jscustomthirdwordrenderdiv.classList.remove("custom-option-font" + i);
            }
        };   
        this.classList.add("selected");
        this.closest('.custom-select').querySelector('.custom-select__trigger span').classList.add("custom-option-" + this.getAttribute("data-value"));
        this.closest('.custom-select').querySelector('.custom-select__trigger span').textContent = this.textContent;
        select.textContent = this.textContent;
        jscustomfirstwordrender.classList.add("custom-option-" + this.getAttribute("data-value"));
        jscustomsecondwordrender.classList.add("custom-option-" + this.getAttribute("data-value"));
        jscustomthirdwordrender.classList.add("custom-option-" + this.getAttribute("data-value"));
        jscustomfirstwordrenderdiv.classList.add("custom-option-" + this.getAttribute("data-value"));
        jscustomsecondwordrenderdiv.classList.add("custom-option-" + this.getAttribute("data-value"));
        jscustomthirdwordrenderdiv.classList.add("custom-option-" + this.getAttribute("data-value"));
    });
    $(d).on("click",".js-variant-value",function() {
        var active = d.querySelectorAll(".js-variant-value");
        var preview = d.querySelector("[js-preview-input]");
        var sizelabel = d.querySelector("[js-product2-size-label");
        [].forEach.call(active, function(el) {
           if(el.classList.contains("active")) {
               el.classList.remove("active");
            }      
        });
        this.classList.add("active");
        var outofstock = d.querySelector("[js-custom-bottom-product-label]");
            if(outofstock){outofstock.classList.remove("custom_order_total_out_of_stock_shake")};
        var variant_selected = d.querySelectorAll(".custom_option_selected");
        if(this.getAttribute("data-option") === "1") {
            variant_selected[0].setAttribute("data-value",this.getAttribute("data-value"));
            variant_selected[0].children[0].src = this.getAttribute("data-imgurl");
            variant_selected[0].children[1].innerHTML = this.getAttribute("data-value");
            preview.setAttribute("data-value",this.getAttribute("data-value"));
        }
        if(this.getAttribute("data-option") === "2") {
            variant_selected[1].setAttribute("data-value",this.getAttribute("data-value"));
            variant_selected[1].children[0].src = this.getAttribute("data-imgurl");
            variant_selected[1].children[1].innerHTML = this.getAttribute("data-value");
            sizelabel.textContent = this.getAttribute("data-size-label");
        }
        if(this.getAttribute("data-option") === "3") {
            variant_selected[2].setAttribute("data-value",this.getAttribute("data-value"));
            variant_selected[2].children[0].src = this.getAttribute("data-imgurl");
            variant_selected[2].children[1].innerHTML = this.getAttribute("data-value");
        }
        var opt1 = variant_selected[0].getAttribute("data-value"), opt2 = variant_selected[1].getAttribute("data-value"), opt3 = variant_selected[2].getAttribute("data-value");
        get_first_variant_id(opt1,opt2,opt3,this);   
    });
    $(d).on("click",".custom-product-option-word-choice", function() {
        var second_variant_id = d.querySelector("[js-second-product-variant-id]");
        var second_variant_check_id = d.querySelector(".customizer__words-input");
        second_variant_id.setAttribute("data-variant-id",this.getAttribute("data-variant-id"));
        second_variant_id.setAttribute("data-variant-price",this.getAttribute("data-variant-price"));
        second_variant_id.setAttribute("data-variant-compare-price",this.getAttribute("data-variant-compare-price"));
        second_variant_id.setAttribute("data-variant-available",this.getAttribute("data-variant-available"));
        second_variant_check_id.setAttribute("data-variant-id",this.getAttribute("data-variant-id"));
        second_variant_check_id.setAttribute("data-variant-price",this.getAttribute("data-variant-price"));
        second_variant_check_id.setAttribute("data-variant-compare-price",this.getAttribute("data-variant-compare-price"));
        second_variant_check_id.setAttribute("data-variant-available",this.getAttribute("data-variant-available"));
        update_total_price();
    });
    function get_first_variant_id(opt1,opt2,opt3,el) {
        var product = d.querySelector("[json-data-product]").innerHTML,
        product = JSON.parse(product || '{}'), first_variant_id = d.querySelector("[js-first-product-variant-id]"), jsvariantfeaturedimg = d.querySelector("[js-variant-featured-img]");
        var product_exist = false;
        var custom_order_total_discount_price = d.querySelector(".custom_order_total_discount_price");
        var customizer_disclaimer_input = d.querySelector(".customizer__disclaimer-input");
        var js_custom_top_product_label = d.querySelector("[js-custom-top-product-label]");
        var custom_order_total_regular_price = d.querySelector(".custom_order_total_regular_price");
        var js_bottom_total_price = d.querySelector("[js-bottom-total-price]");
        var custom_label_regular_price = d.querySelector(".custom_label_regular_price");
        var custom_order_bottom_total_regular_price = d.querySelector(".custom_order_bottom_total_regular_price");
        var custom_label_sale_price = d.querySelector(".custom_label_sale_price");
        var custom_order_bottom_total_discount_price = d.querySelector(".custom_order_bottom_total_discount_price");
        var custom_order_bottom_total_saved_price  = d.querySelector(".custom_order_bottom_total_saved_price");
        var custom_order_text_left = d.querySelector(".custom-order-text-left");
        for(i=0;i<product.variants.length;i++) {
            if(product.variants[i].option1 === opt1 && product.variants[i].option2 === opt2 && product.variants[i].option3 === opt3) {             
                        if(product.variants[i].featured_image != null) {
                        jsvariantfeaturedimg.src = product.variants[i].featured_image.src;
                        }
                        first_variant_id.setAttribute("data-variant-id",product.variants[i].id);
                        if(product.variants[i].price) {
                        first_variant_id.setAttribute("data-variant-price",product.variants[i].price);
                        } else {
                        first_variant_id.setAttribute("data-variant-price",0);    
                        }
                        if(product.variants[i].compare_at_price) {
                        first_variant_id.setAttribute("data-variant-compare-price",product.variants[i].compare_at_price);
                        } else {
                        first_variant_id.setAttribute("data-variant-compare-price",0);
                        }
                        if(product.variants[i].available) {
                        first_variant_id.setAttribute("data-variant-available","true");    
                        var variant_status_props_all = d.querySelectorAll('.variant-status-props');
                        [].forEach.call(variant_status_props_all,function(e) {
                            e.classList.add('d-none');
                        });                          
                        } else {
                            first_variant_id.setAttribute("data-variant-available","false");   
                            el.children[0].classList.remove('d-none');
                        }
                        product_exist = true;
                        update_total_price();          
            }
        }
        if(product_exist) {
            custom_order_total_discount_price.classList.remove("d-none");
            if(custom_label_regular_price){custom_label_regular_price.textContent = "Regular Price";}
            if(custom_label_sale_price){custom_label_sale_price.textContent = "Sale Price";}
            if(custom_order_text_left){custom_order_text_left.textContent = "Total Price";}       
        } else {
            custom_order_total_discount_price.classList.add("d-none");
            customizer_disclaimer_input.disabled = true;
            if(js_custom_top_product_label){js_custom_top_product_label.classList.remove("d-none");}
            if(js_custom_top_product_label){js_custom_top_product_label.textContent = "NOT AVAILABLE";}
            if(custom_order_total_regular_price){custom_order_total_regular_price.classList.add("d-none");}
            if(js_bottom_total_price){js_bottom_total_price.textContent = "";}
            if(custom_label_regular_price){custom_label_regular_price.textContent = "";}
            if(custom_order_bottom_total_regular_price){custom_order_bottom_total_regular_price.textContent = "";}
            if(custom_label_sale_price){custom_label_sale_price.textContent = "";}
            if(custom_order_bottom_total_discount_price){custom_order_bottom_total_discount_price.textContent = "";}
            if(custom_order_bottom_total_saved_price){custom_order_bottom_total_saved_price.textContent = "NOT AVAILABLE";}
            if(custom_order_text_left){custom_order_text_left.textContent = "";}
        }
    }
    function update_total_price() {
        var first_variant_price = d.querySelector("[js-first-product-variant-id]");
        var second_variant_price = d.querySelector("[js-second-product-variant-id]");
        var price_currency = d.querySelector("[js-current-current]").getAttribute("data-currency");
        var total_price = 0;
        var price = d.querySelector(".custom_order_total_discount_price");
        var compare = d.querySelector(".custom_order_total_regular_price");
        var producttoplabel = d.querySelector("[js-custom-top-product-label]");
        var bottomprice = d.querySelector("[js-bottom-total-price]");
        var bottomcompareprice = d.querySelector(".custom_order_bottom_total_regular_price");
        var bottomdiscountprice = d.querySelector(".custom_order_bottom_total_discount_price");
        var disclaimer = d.querySelector(".customizer__disclaimer-input");
        var productbottomlabel = d.querySelector("[js-custom-bottom-product-label]");
        var customorderaddtocart = d.querySelector(".custom-order-add-to-cart");
        var custom_order_bottom_total_regular_price = d.querySelector(".custom_order_bottom_total_regular_price");
        var custom_label_sale_price = d.querySelector(".custom_label_sale_price");
        var custom_order_bottom_total_saved_price = d.querySelector(".custom_order_bottom_total_saved_price");
        var first_variant_compare_price = 0, first_variant_regular_price = 0, second_variant_compare_price = 0, second_variant_regular_price = 0, first_variant_available, second_variant_available;
        price_currency = price_currency.replace("0.00", "");
        if(first_variant_price.getAttribute("data-variant-price")) {
            first_variant_regular_price = Number(first_variant_price.getAttribute("data-variant-price")) * 0.01;
            first_variant_compare_price = Number(first_variant_price.getAttribute("data-variant-compare-price")) * 0.01;
        } else {
            first_variant_regular_price = Number(0);
            first_variant_compare_price = Number(0);
        }
        if(second_variant_price) {
            second_variant_regular_price = Number(second_variant_price.getAttribute("data-variant-price")) * 0.01;
            second_variant_compare_price = Number(second_variant_price.getAttribute("data-variant-compare-price")) * 0.01;
        } else {
            second_variant_regular_price = Number(0);
            second_variant_compare_price = Number(0);
        }
        first_variant_available = first_variant_price.getAttribute("data-variant-available");
        second_variant_available = second_variant_price.getAttribute("data-variant-available");
        total_price = Number(first_variant_regular_price) + Number(second_variant_regular_price);
        total_compare_price = Number(first_variant_compare_price) + Number(second_variant_compare_price);
        total_saved_price = Number(total_compare_price) - Number(total_price);
        price.textContent = price_currency + total_price.toFixed(2);
        if(compare){compare.textContent = price_currency + total_compare_price.toFixed(2);}
        bottomprice.textContent = price_currency + total_price.toFixed(2);
        if(bottomcompareprice){bottomcompareprice.textContent = price_currency + total_compare_price.toFixed(2);}
        if(bottomdiscountprice && total_price < total_compare_price) {
            bottomdiscountprice.textContent = price_currency + total_price.toFixed(2);
        } else {
            var zero_discount = 0;
            bottomdiscountprice.textContent = price_currency + zero_discount.toFixed(2);
        }   
        if(first_variant_available === 'true') {
            if(producttoplabel){producttoplabel.classList.remove("custom_order_total_out_of_stock");}
            if(producttoplabel){producttoplabel.classList.add("custom_order_total_saved_price");}
            if(producttoplabel){producttoplabel.textContent = 'SAVE ' + price_currency + total_saved_price.toFixed(2);}
            disclaimer.disabled = false;
            disclaimer.checked = false;
            customorderaddtocart.classList.add("disabled");
            if(productbottomlabel){productbottomlabel.classList.remove("d-none");}
            if(productbottomlabel){productbottomlabel.classList.remove("custom_order_total_out_of_stock");}
            if(productbottomlabel){productbottomlabel.classList.add("custom_order_total_saved_price");}
            if(productbottomlabel){productbottomlabel.textContent = 'SAVE ' + price_currency + total_saved_price.toFixed(2);}              
        } else {
            if(producttoplabel){producttoplabel.classList.add("custom_order_total_out_of_stock");}
            if(producttoplabel){producttoplabel.classList.remove("custom_order_total_saved_price");}    
            if(producttoplabel){producttoplabel.textContent = 'OUT OF STOCK';}   
            disclaimer.disabled = true;
            disclaimer.checked = false;
            customorderaddtocart.classList.add("disabled");
            if(productbottomlabel){productbottomlabel.classList.add("custom_order_total_out_of_stock");}
            if(productbottomlabel){productbottomlabel.classList.remove("custom_order_total_saved_price");}
            if(productbottomlabel){productbottomlabel.textContent = 'OUT OF STOCK'; } 
        }
        if(total_saved_price>0) {
            if(compare){compare.classList.remove("d-none");}
            if(producttoplabel){producttoplabel.classList.remove("d-none");}
            if(custom_order_bottom_total_regular_price){custom_order_bottom_total_regular_price.classList.remove("d-none");}
            if(custom_label_sale_price){custom_label_sale_price.classList.remove("d-none");}
            if(custom_order_bottom_total_saved_price){custom_order_bottom_total_saved_price.classList.remove("d-none");}
        } else {
            if(compare){compare.classList.add("d-none");}
            if(producttoplabel){producttoplabel.classList.add("d-none");}
            if(custom_order_bottom_total_regular_price){custom_order_bottom_total_regular_price.classList.add("d-none");}
            if(custom_label_sale_price){custom_label_sale_price.classList.add("d-none");}
            if(custom_order_bottom_total_saved_price){custom_order_bottom_total_saved_price.classList.add("d-none");}
        }
    }
    $(d).on("click",".custom-order-review-button", function(){
            var first_variant_id = d.querySelector("[js-first-product-variant-id]");
            var second_variant_id = d.querySelector("[js-second-product-variant-id]");
            var second_variant_check_id = d.querySelector(".customizer__words-input");
            var first_word = d.querySelector(".js-custom-first-word");
            var second_word = d.querySelector(".js-custom-second-word");
            var third_word = d.querySelector(".js-custom-third-word");
            var selected_word_variant = d.querySelector(".custom_words_variant_mr");
            var font_family = d.querySelector(".custom-product-option-fonts-selected");
            var engraving_services = d.querySelector(".no-engraving-services");
            var engraving_batch_id = Date.now() / 1000 | 0;
            var itemObj = [];
            let formData;
                    itemObj.push(first_variant_id.getAttribute("data-variant-id"));
                    itemObj.push(second_variant_id.getAttribute("data-variant-id"));

                    if(second_variant_check_id.checked) {
                        
                        if(selected_word_variant.textContent === 'One word') {
                            formData = {
                                'items': [  
                                        {
                                        'id': itemObj[0],
                                        'quantity': 1,
                                        'properties': {
                                            'Disclaimer': 'accepted',
                                            'Personal Engraving' : engraving_services.textContent,
                                            'Batch ID': engraving_batch_id
                                        } 
                                        },
                                        {
                                        'id': itemObj[1],
                                        'quantity': 1,
                                        'properties': {
                                            'First Word': first_word.value,
                                            'Font Family': font_family.textContent,
                                            'Batch ID': engraving_batch_id                                                     
                                          }
                                        }
                                     ]
                               };
                        }
                        if(selected_word_variant.textContent === 'Two words') {
                            formData = {
                                'items': [  
                                        {
                                        'id': itemObj[0],
                                        'quantity': 1,
                                        'properties': {
                                            'Disclaimer': 'Accepted',
                                            'Personal Engraving' : engraving_services.textContent,
                                            'Batch ID': engraving_batch_id
                                        } 
                                        },
                                        {
                                        'id': itemObj[1],
                                        'quantity': 1,
                                        'properties': {
                                            'First word': first_word.value,
                                            'Second word': second_word.value,
                                            'Font Family': font_family.textContent,
                                            'Batch ID': engraving_batch_id                                                        
                                          }
                                        }
                                     ]
                               };
                        }
                        if(selected_word_variant.textContent === 'Three words') {
                            formData = {
                                'items': [  
                                        {
                                        'id': itemObj[0],
                                        'quantity': 1,
                                        'properties': {
                                            'Disclaimer': 'Accepted',
                                            'Personal Engraving' : engraving_services.textContent,
                                            'Batch ID': engraving_batch_id
                                        } 
                                        },
                                        {
                                        'id': itemObj[1],
                                        'quantity': 1,
                                        'properties': {
                                            'First word': first_word.value,
                                            'Second word': second_word.value,
                                            'Third word': third_word.value,
                                            'Font Family': font_family.textContent,
                                            'Batch ID': engraving_batch_id                                                       
                                          }
                                        }
                                     ]
                               };
                        }
                    } else {
                        formData = {
                            'items': [  
                                    {
                                    'id': itemObj[0],
                                    'quantity': 1,
                                    'properties': {
                                        'Disclaimer': 'Accepted',
                                        'Personal Engraving' : engraving_services.textContent
                                    }
                                    }
                                 ]
                           };
                    }
                    $.ajax({
                            type: 'post',
                            url: '/cart/add.js',
                            data: formData,
                            dataType: 'json',
                            success: function() { 
                               window.location.reload();
                            }
                        });  
    });
    $(d).on("keyup", ".js-custom-first-word", function() {
        var first_render = d.querySelector(".js-custom-first-word-render");
        var first_render_div = d.querySelector(".js-custom-first-word-render-div")
        first_render.textContent = this.value;
        first_render_div.textContent = this.value;
    });
    $(d).on("keyup", ".js-custom-second-word", function() {
        var second_render = d.querySelector(".js-custom-second-word-render");
        var second_render_div = d.querySelector(".js-custom-second-word-render-div");
        second_render.textContent = this.value;
        second_render_div.textContent = this.value;
    });
    $(d).on("keyup", ".js-custom-third-word", function() {
        var third_render = d.querySelector(".js-custom-third-word-render");
        var third_render_div = d.querySelector(".js-custom-third-word-render-div");
        third_render.textContent = this.value;
        third_render_div.textContent = this.value;
    });
    $(d).on("focus", ".js-custom-first-word", function() {
      this.style.setProperty("background-color","white");
    });
    $(d).on("focus", ".js-custom-second-word", function() {
        this.style.setProperty("background-color","white");
    });
    $(d).on("focus", ".js-custom-third-word", function() {
        this.style.setProperty("background-color","white");
    });
    $(d).on("keypress", ".js-custom-first-word", function(e) {
        var disclaimer = d.querySelector(".customizer__disclaimer-input");
        var btn_add_to_cart = d.querySelector(".custom-order-add-to-cart");
        var checkinput = d.querySelector(".customizer__words-input");
        if(e.keyCode == 8 || e.keyCode == 46) {
            if(this.value === '' && checkinput.checked) {
            disclaimer.checked = false
            btn_add_to_cart.classList.add("disabled");
            }
        }
        if(e.which === 32 || this.value === ' ') {
            return false;
        }
    });
    $(d).on("keypress", ".js-custom-second-word", function(e) {
        var disclaimer = d.querySelector(".customizer__disclaimer-input");
        var btn_add_to_cart = d.querySelector(".custom-order-add-to-cart");
        var checkinput = d.querySelector(".customizer__words-input");
        if(e.keyCode == 8 || e.keyCode == 46) {
            if(this.value === '' && checkinput.checked) {
            disclaimer.checked = false
            btn_add_to_cart.classList.add("disabled");
            }
        }
        if(e.which === 32 || this.value === ' ') {
            return false;
        }
    });
    $(d).on("keypress", ".js-custom-third-word", function(e) {
        var disclaimer = d.querySelector(".customizer__disclaimer-input");
        var btn_add_to_cart = d.querySelector(".custom-order-add-to-cart");
        var checkinput = d.querySelector(".customizer__words-input");
        if(e.keyCode == 8 || e.keyCode == 46) {
            if(this.value === '' && checkinput.checked) {
            disclaimer.checked = false
            btn_add_to_cart.classList.add("disabled");
            }
        }
        if(e.which === 32 || this.value === ' ') {
            return false;
        }
    });
    $(d).on("click", ".customizer__disclaimer-input", function() {
        var btn_add_to_cart = d.querySelector(".custom-order-add-to-cart");
        var first_word = d.querySelector(".js-custom-first-word");
        var second_word = d.querySelector(".js-custom-second-word");
        var third_word = d.querySelector(".js-custom-third-word");
        var js_preview_input = d.querySelector("[js-preview-input]");
        var custom_product_option_word = d.querySelector(".custom-product-option-word");
        var selected_word_variant = d.querySelector(".custom_words_variant_mr");
        var custom_product_option_word_input = d.querySelector(".custom-product-option-word-input");
        var disclaimer = d.querySelector(".customizer__disclaimer-input");
        var optiondiv = d.querySelectorAll(".custom-product-option-div");
        var checkinput = d.querySelector(".customizer__words-input");
        for(i=0;i<optiondiv.length;i++) {
           if(optiondiv[i].classList.contains("active")){
               optiondiv[i].classList.remove("active");
               for(x=0;x<optiondiv[i].children.length;x++) {
                   if(x>0) {
                   optiondiv[i].children[x].classList.add("d-none");
                   }
               }
           };
        }
        if(selected_word_variant.textContent === "One word" && checkinput.checked) {
            if(first_word.value !== '') {
                if(this.checked) {       
                    btn_add_to_cart.classList.remove("disabled");
                } else {
                    btn_add_to_cart.classList.add("disabled");
                }
            } else {
                disclaimer.checked = false;
                js_preview_input.classList.add("active");
                btn_add_to_cart.classList.add("disabled");
                custom_product_option_word.classList.remove("d-none");
                custom_product_option_word_input.classList.remove("d-none");
                location.href = "#words_option";
                first_word.style.setProperty("background-color","red");
            }
        } else if(this.checked) {
            btn_add_to_cart.classList.remove("disabled");
        } else {
            btn_add_to_cart.classList.add("disabled");
        }
        if(selected_word_variant.textContent === "Two words" && checkinput.checked) {
            if(first_word.value !== '' && second_word.value !== '') {
                if(this.checked) {       
                    btn_add_to_cart.classList.remove("disabled");
                } else {
                    btn_add_to_cart.classList.add("disabled");
                }
            } else if(checkinput.checked) {
                disclaimer.checked = false;
                location.href = "#words_option";
                first_word.style.setProperty("background-color","red");
                second_word.style.setProperty("background-color","red");
                btn_add_to_cart.classList.add("disabled");
                js_preview_input.classList.add("active");
                custom_product_option_word.classList.remove("d-none");
                custom_product_option_word_input.classList.remove("d-none");
            }
        } else if(this.checked) {
            btn_add_to_cart.classList.remove("disabled");
        } else {
            btn_add_to_cart.classList.add("disabled");
        }
        if(selected_word_variant.textContent === "Three words" && checkinput.checked) {
            if(first_word.value !== '' && second_word.value !== '' && third_word.value !== '') {
                if(this.checked) {       
                    btn_add_to_cart.classList.remove("disabled");
                } else {
                    btn_add_to_cart.classList.add("disabled");
                }
            } else if(checkinput.checked) {
                disclaimer.checked = false;
                location.href = "#words_option";
                first_word.style.setProperty("background-color","red");
                second_word.style.setProperty("background-color","red");
                third_word.style.setProperty("background-color","red");
                btn_add_to_cart.classList.add("disabled");
                js_preview_input.classList.add("active");
                custom_product_option_word.classList.remove("d-none");
                custom_product_option_word_input.classList.remove("d-none");
            }
        } else if(this.checked) {
            btn_add_to_cart.classList.remove("disabled");
        } else {
            btn_add_to_cart.classList.add("disabled");
        }
    });
    $(d).on("click",".customizer__words-input", function() {
        var preview = d.querySelector("[js_custom_font_text_value]");
        var second_variant_id = d.querySelector("[js-second-product-variant-id]"); 
        var note_engraving = d.querySelector(".custom_order_note_engraving");
        var popup = d.querySelector("[custom-order-popup");
        var modal_backdrop = d.querySelector(".modal-backdrop");
        var no_engraving = d.querySelector(".no-engraving-services");
       
        if(this.checked) {         
            second_variant_id.setAttribute("data-variant-id",this.getAttribute("data-variant-id"));
            second_variant_id.setAttribute("data-variant-price",this.getAttribute("data-variant-price"));
            second_variant_id.setAttribute("data-variant-compare-price",this.getAttribute("data-variant-compare-price"));
            preview.classList.remove("d-none");
            note_engraving.classList.remove("d-none");
            popup.classList.remove("show");
            popup.classList.add("d-none");
            modal_backdrop.classList.remove("show");
            modal_backdrop.classList.add("d-none");
            no_engraving.textContent = m8.yes_engraving_confirmation;
            update_total_price();
        } else {
            second_variant_id.setAttribute("data-variant-id",this.getAttribute("data-variant-id"));
            second_variant_id.setAttribute("data-variant-price", 0);
            second_variant_id.setAttribute("data-variant-compare-price", 0);
            preview.classList.add("d-none");
            note_engraving.classList.add("d-none");
            popup.classList.add("show");
            popup.classList.remove("d-none");
            modal_backdrop.classList.add("show");
            modal_backdrop.classList.remove("d-none");
            no_engraving.textContent = no_engraving_confirmation;
            update_total_price();
        }
    });
    $(d).on("click", ".checkmark", function() {
        var checkbox = d.querySelector(".customizer__disclaimer-input");
        var outofstock = d.querySelector("[js-custom-bottom-product-label]");
        if(checkbox.disabled) {        
            if(outofstock){outofstock.classList.add("custom_order_total_out_of_stock_shake");}
        } else {
            if(outofstock){outofstock.classList.remove("custom_order_total_out_of_stock_shake");}
        }
    });
    $('.custom-order-product-form').scroll(function() {
        var sticky_header = d.querySelector("[js-custom-order-sticky-header]");
            if(this.scrollTop>188 && sticky_header.style.position !== "absolute") {       
                sticky_header.style.setProperty("position", "absolute");
                sticky_header.style.setProperty("width", "100%");
                sticky_header.style.setProperty("background", "#fff0e3");
                sticky_header.style.setProperty("z-index", "999");
                sticky_header.style.setProperty("top", "-2px");               
            }
            if(this.scrollTop<88 && sticky_header.style.position === "absolute") {       
                sticky_header.style.removeProperty("position");
                sticky_header.style.removeProperty("width");
                sticky_header.style.removeProperty("background");
                sticky_header.style.removeProperty("z-index");
                sticky_header.style.removeProperty("top");
            }
    });
    $(d).scroll(function() {
        var sticky_custom_order = d.querySelector("#shopify-section-custom-order");
        var sticky_header_form = d.querySelector('.custom-order-form-w40');   
        if (m8.matchMedia('(max-width: 767px)').matches) {  
                if(m8.pageYOffset>188 && sticky_custom_order && sticky_custom_order.style.position !== "fixed") {       
                    sticky_custom_order.style.setProperty("position", "fixed");
                    sticky_custom_order.style.setProperty('z-index', '999');
                    sticky_custom_order.style.setProperty('top', '-23px'); 
                    sticky_header_form.style.setProperty('margin-top','-28px');    
                }   
                if(m8.pageYOffset<88 && sticky_custom_order && sticky_custom_order.style.position === 'fixed') {
                    sticky_custom_order.style.removeProperty("position");
                    sticky_header_form.style.removeProperty("margin-top");
                    sticky_custom_order.style.removeProperty("z-index");
                    sticky_custom_order.style.removeProperty("top"); 
                }
        }
    });
    /* product swatch selection start here */
    $(d).on("click", ".swatches a", function() {
        var url = this.children[0].children[0].style.backgroundImage;
        var product_customiser_button = d.querySelector(".product_customiser_button a");
        if (url.includes('BWG.jpg') && product_customiser_button) {
            setParamURL(product_customiser_button.href, 'option3', 'Black | White | Gold');
        };
        if (url.includes('NWM.jpg') && product_customiser_button) {
            setParamURL(product_customiser_button.href,'option3','Navy | White | Metallic');
        };
        if (url.includes('TWG.jpg') && product_customiser_button) {
            setParamURL(product_customiser_button.href,'option3','Teal | White | Gold');
        };
        if (url.includes('WGG.jpg') && product_customiser_button) {
            setParamURL(product_customiser_button.href,'option3','White | Grey | Gold');
        };
        if (url.includes('RWG.jpg') && product_customiser_button) {
            setParamURL(product_customiser_button.href,'option3','Raspberry | White | Gold');
        };
        if (url.includes('EJ.jpg') && product_customiser_button) {
            setParamURL(product_customiser_button.href,'option3','Emerald Jewel');
        };
        if (url.includes('RG.jpg') && product_customiser_button) {
            setParamURL(product_customiser_button.href,'option3','Rose Gold');
        };    
    });
    /* product swatch selection end here */
    function setParamURL(url_string, param, value) {
        var url = new URL(url_string);   
        var product_customiser_button = d.querySelector(".product_customiser_button a");
        url.searchParams.set(param, value);
        product_customiser_button.href = url;
    };
    function getParamURL(url_string, param) {
        var url = new URL(url_string);    
        var param = url.searchParams.get(param);
        return param;
    };
    m8.repopulate_selected_options = function() {
          var js_variant_value = d.querySelectorAll(".js-variant-value");
          var url = m8.location.href;
          var option1 = getParamURL(url,"option1");
          var option2 = getParamURL(url,"option2");
          var option3 = getParamURL(url,"option3");
          if(option1) {
          [].forEach.call(js_variant_value, function(el) {
            if(el.getAttribute('data-value') === option1) {
              el.click();
            }   
          });
          };
          if(option2) {
          [].forEach.call(js_variant_value, function(el) {         
              var pp = option2.replace(' (Acacia Only)', '');
              var pl = el.getAttribute('data-value').replace('  (Acacia Only)', '');
            if(pl === pp) {
              el.click();
            }
          });
          };
          if(option3) {
          [].forEach.call(js_variant_value, function(el) {
            if(el.getAttribute('data-value') === option3) {
              el.click();
            }
          });
          };
    }
    $(d).on("click", ".popup_btn_continue", function() {
        var popup = d.querySelector("[custom-order-popup]");
        var modal_backdrop = d.querySelector(".modal-backdrop");
        var no_engraving = d.querySelector(".no-engraving-services");
        popup.classList.remove("show");
        popup.classList.add("d-none");
        modal_backdrop.classList.remove("show");
        modal_backdrop.classList.add("d-none");
        no_engraving.textContent = no_engraving_confirmation;
    });
    $(d).on("click", ".popup_btn_back", function() {
        var popup = d.querySelector("[custom-order-popup]");
        var modal_backdrop = d.querySelector(".modal-backdrop");
        var engraving = d.querySelector(".customizer__words-input");
        var no_engraving = d.querySelector(".no-engraving-services");
        popup.classList.remove("show");
        popup.classList.add("d-none");
        modal_backdrop.classList.remove("show");
        modal_backdrop.classList.add("d-none");
        no_engraving.textContent = m8.yes_engraving_confirmation;
        engraving.click();
    });
    $(d).on("click",".cart-drawer__item-delete", function() {
        let formData = 'updates['+this.getAttribute("data-primary-item")+']=0&updates['+this.getAttribute("data-secondary-item")+']=0';
        $.post('/cart/update.js', formData, function() {
            window.theme.cart._promiseChange({
                url: "/cart.json",
                dataType: "json"
            });
            window.location.reload();
        }); 
    });
    /*start here variant roll-over second image */
    $(d).on("mouseover", ".card__image-wrapper .card__image", function() {
        ri = this.parentElement;
        rin = ri.getElementsByTagName('img');
        if(rin[0]){rin[0].classList.add("product-image-d-none");}
        if(rin[0]){rin[0].classList.remove("product-image-d-block");}
        if(rin[2]){rin[2].classList.remove("product-image-d-none");}
        if(rin[2]){rin[2].classList.add("product-image-d-block");}
    });
    $(d).on("mouseout", ".card__image-wrapper .card__image", function() {
        ri = this.parentElement;
        rin = ri.getElementsByTagName('img');
        if(rin[0]){rin[0].classList.remove("product-image-d-none");}
        if(rin[0]){rin[0].classList.add("product-image-d-block");}
        if(rin[2]){rin[2].classList.add("product-image-d-none");}
        if(rin[2]){rin[2].classList.remove("product-image-d-block");}
    });
    $(d).on("mouseover", ".product__media-wrapper .product__media-preview-image", function() {
        ri = this.parentElement;
        rin = ri.getElementsByTagName('img');
        if(rin[0]){rin[0].classList.add("product-image-d-none");}
        if(rin[0]){rin[0].classList.remove("product-image-d-block");}
        if(rin[1]){rin[1].classList.remove("product-image-d-none");}
        if(rin[1]){rin[1].classList.add("product-image-d-block");}
    });
    $(d).on("mouseout", ".product__media-wrapper .product__media-preview-image", function() {
        ri = this.parentElement;
        rin = ri.getElementsByTagName('img');
        if(rin[0]){rin[0].classList.remove("product-image-d-none");}
        if(rin[0]){rin[0].classList.add("product-image-d-block");}
        if(rin[1]){rin[1].classList.add("product-image-d-none");}
        if(rin[1]){rin[1].classList.remove("product-image-d-block");}
    });
    /*end here variant roll-over second image */
    /* start here variant color option */
    $(d).on("click", ".js-swatch-value", function() {
        var product_img = d.querySelector('[card-image-data-id="' + this.getAttribute("card-image-data-id") + '"]');
        var engrave_url = '/pages/custom-order?';
        var btn_engrave_url = d.querySelector('.product_customiser_button a');
        var contact_variant_url = d.querySelector('[name="contact[selected_product_variant_url]"]');
        var variant_id = this.getAttribute("data-variant-id");
        var card_image_data_id = d.querySelectorAll('[card-image-data-id="' + this.getAttribute("card-image-data-id") + '"]');
        var filtered_variant = m8.pso_obj_variant_images.variants.filter(function(data) { return data.id === variant_id; });
        var js_variant_url = d.querySelector('[js-variant-url="' + this.getAttribute("card-image-data-id") + '"]');
        var js_variant_title = d.querySelector('[js-variant-title="' + this.getAttribute("card-image-data-id") + '"]');
        var select_option_item = d.querySelectorAll(".select-option-item");
        var swatches_item = d.querySelectorAll('[card-image-data-id="' + this.getAttribute("card-image-data-id") + '"].swatch');
        var select_selected = d.querySelector(".select-selected");
        var variant_selected = d.querySelector("[data-master-select]");
        var product__media_container = d.querySelectorAll('.product__media-container');
        this.classList.add("active");
        if(window.location.href.includes('products')) {
           if(filtered_variant[0].img) {
            filtered_variant[0].img.classList.add("product__media-preview-image");
           }
           [].forEach.call(swatches_item, function(el,index) {
            if(el.classList.contains("active")) {
                if(select_option_item[index]) {
                    select_option_item[index].classList.add("active");
                select_selected.textContent = select_option_item[index].textContent;
                variant_selected.selectedIndex = index;
                variant_selected.options[index].setAttribute("selected", "selected");
                }          
            } else {
                if(select_option_item[index]) {
                select_option_item[index].classList.remove("active");
                variant_selected.options[index].removeAttribute("selected");
                }           
            };
           });
           [].forEach.call(card_image_data_id, function(el) {
            if(el.classList.contains("active")) {
                el.classList.remove("active");
            }
            });
            [].forEach.call(product__media_container, function(el, i) {
                if(i == 0) {
                    el.classList.remove('hide');
                } else {
                    el.classList.add('hide');
                }
            });
        };
        if(product_img && filtered_variant){product_img.replaceChild(filtered_variant[0].img, product_img.children[0])};    
        if(js_variant_url){js_variant_url.href = this.getAttribute("data-variant-url")};
        if(js_variant_title){js_variant_title.href = this.getAttribute("data-variant-url")};    
        if(contact_variant_url){contact_variant_url.value = 'https://' +window.location.hostname + this.getAttribute("data-variant-url")};
        if(btn_engrave_url){btn_engrave_url.href = engrave_url + 'option1='+ this.getAttribute('data-option1') + '&option2=' + this.getAttribute('data-option2') + '&option3=' + this.getAttribute('data-option3')};
    });
    /* end here variant color option */
    $(d).on("click", ".tab .tablinks", function() {
        var tabcontent = d.querySelectorAll(".tabcontent");
        tabcontent[this.getAttribute("tab-index")].classList.toggle("active");
        this.classList.toggle("active");
    });
    $(d).on("click", ".select-selected", function() {
    var select_items = d.querySelector(".select-items");
    select_items.classList.toggle("d-none"); 
    this.classList.toggle("open");
    });
    $(d).on("click", ".select-option-item", function() {
        var select_selected = d.querySelector(".select-selected");
        var select_items = d.querySelector(".select-items");
        var select_option_item = d.querySelectorAll(".select-option-item");
        var swatches_item = d.querySelectorAll('[card-image-data-id="' + this.getAttribute("card-image-data-id") + '"].swatch');
        var variant_selected = d.querySelector("[data-master-select]");
        select_selected.textContent = this.textContent;
        select_items.classList.add("d-none"); 
        [].forEach.call(select_option_item, function(el) {
            if(el.classList.contains("active")) {
                el.classList.remove("active");
            }
        });    
        this.classList.add("active");
        if(window.location.href.includes('products')) {
            [].forEach.call(select_option_item, function(el,index) {
             if(el.classList.contains("active")) {
                swatches_item[index].classList.add("active");
                variant_selected.selectedIndex = index;
                variant_selected.options[index].setAttribute("selected", "selected");
             } else {
                swatches_item[index].classList.remove("active");
                variant_selected.options[index].removeAttribute("selected");
             };
            });
         };
    });
    $(d).on("click", '.product__add-to-cart-button-ajax', function(event) {
        var checkboxupsell = $('.product_upsell_variant').prop('checked'),
            checkboxgift = $('.product_gift_checkbox').prop('checked'),
            inputto = $('.product_gift_input_to').val(),
            inputfrom = $('.product_gift_input_from').val(),
            inputmsg = $('.product_gift_input_msg').val(),
            upsellid = $('.product_upsell_variant').val(),
            variantid = $('.product-form__master-select').val(),
            variantqty = $('[name="quantity"').val(),
            options = 0;
        if(typeof checkboxupsell !== 'undefined') {
        if(checkboxupsell == false && checkboxgift == false){options = 0;}
        if(checkboxupsell && checkboxgift == false){options = 1;}
        if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg !== ''){options = 3;}
        if(checkboxupsell && checkboxgift && inputto === '' && inputfrom === '' && inputmsg === ''){options = 'default';}
        if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
        if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
        if(checkboxupsell && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg !== ''){options = 'default';}
        if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
        if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
        if(checkboxupsell && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
        if(checkboxupsell && checkboxgift && inputto === '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
        if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom === '' && inputmsg === ''){options = 'default';}
        if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
        if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
        if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg !== ''){options = 'default';}
        if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
        if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
        if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
        if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg !== ''){options = 2;}
        }; 
        if(typeof checkboxupsell === 'undefined') {
        if(checkboxgift && inputto === '' && inputfrom === '' && inputmsg === ''){options = 'default';}
        if(checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
        if(checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
        if(checkboxgift && inputto === '' && inputfrom !== '' && inputmsg !== ''){options = 'default';}
        if(checkboxgift && inputto === '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
        if(checkboxgift && inputto !== '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
        if(checkboxgift && inputto === '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
        if(checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg !== ''){options = 2;}
        }
        switch(options) {
            case 0:
              // both without upsell and gift form
              SendtoCart(0);
              break;
            case 1:
              // with upsell without gift form
              event.preventDefault();
              SendtoCart(1);
              break;
            case 2:
              // without upsell but with gift form
              event.preventDefault();
              SendtoCart(2);
              break;
            case 3:
              // both with upsell && gift form
              event.preventDefault();
              SendtoCart(3);
              break;
            default:
           // no upsell but gift form required fields are empty
           if(inputto === '') {
            event.preventDefault();
          $('.product_gift_input_to').focus();
            return false;
          }
          if(inputfrom === '') {
            event.preventDefault();
            $('.product_gift_input_from').focus();
            return false;
          }
          if(inputmsg === '') {
            event.preventDefault();
            $('.product_gift_input_msg').focus();
            return false;
          }
}    
    function SendtoCart(option) {
        let formData;
        //option 0
        if(option == 0) {       
            $(this).click();
            window.theme.sections.instances[0]._openCartDrawer(new Event('click'));  
            CheckCartItem();
            return true;
        }  
        //option 1
        if(option == 1) {
            formData = {
                'items': [  
                            {
                            'id': upsellid,
                            'quantity': 1,
                            'properties': {
                                'Upsell Item': 'Accepted',
                                'Upsell from Variant ID': variantid,
                                'Batch ID': Date.now() / 1000 | 0
                                }
                            },
                            {
                            'id': variantid,
                            'quantity': variantqty,
                            'properties': {
                                'Batch ID': Date.now() / 1000 | 0
                                }
                            }
                     ]
               };
               $.ajax({
                type: 'POST',
                url: '/cart/add.js',
                data: formData,
                dataType: 'json',
                success: function() {
                    window.theme.cart._promiseChange({
                        url: "/cart.json",
                        dataType: "json"
                    });
                    window.theme.sections.instances[0]._openCartDrawer(new Event('click'));  
                    CheckCartItem();
                    return false;  
                }
                }); 
        }
        //option 2
        if(option == 2) {
            formData = {
                'items': [  
                            {
                            'id': variantid,
                            'quantity': variantqty,
                            'properties': {
                                'Gift': 'Yes',
                                'To': inputto,
                                'From': inputfrom,
                                'Messages': inputmsg
                                }
                            }
                     ]
               };
               $.ajax({
                type: 'POST',
                url: '/cart/add.js',
                data: formData,
                dataType: 'json',
                success: function() {
                    window.theme.cart._promiseChange({
                        url: "/cart.json",
                        dataType: "json"
                    });
                    CheckCartItem();
                    window.theme.sections.instances[0]._openCartDrawer(new Event('click'));  
                    return false;  
                }
                }); 
        }
        //option 3
        if(option == 3) {
            formData = {
                'items': [  
                            {
                            'id': upsellid,
                            'quantity': 1,
                            'properties': {
                                'Upsell Item': 'Accepted',
                                'Upsell from Variant ID': variantid,
                                'Batch ID': Date.now() / 1000 | 0
                                }
                            },
                            {
                            'id': variantid,
                            'quantity': variantqty,
                            'properties': {
                                'Gift': 'Yes',
                                'To': inputto,
                                'From': inputfrom,
                                'Messages': inputmsg,
                                'Batch ID': Date.now() / 1000 | 0
                                }
                            }
                     ]
               };
               $.ajax({
                type: 'POST',
                url: '/cart/add.js',
                data: formData,
                dataType: 'json',
                success: function() {
                    window.theme.cart._promiseChange({
                        url: "/cart.json",
                        dataType: "json"
                    });
                    CheckCartItem();
                    window.theme.sections.instances[0]._openCartDrawer(new Event('click'));  
                    return false;  
                }
                }); 
        }
    }     
    });
    $(d).on('click', '[data-testid="PayPalInContext-button"]', function(event) {
        var checkboxupsell = $('.product_upsell_variant').prop('checked'),
        checkboxgift = $('.product_gift_checkbox').prop('checked'),
        inputto = $('.product_gift_input_to').val(),
        inputfrom = $('.product_gift_input_from').val(),
        inputmsg = $('.product_gift_input_msg').val(),
        upsellid = $('.product_upsell_variant').val(),
        variantid = $('.product-form__master-select').val(),
        variantqty = $('[name="quantity"').val(),
        options = 0;
        if(typeof checkboxupsell !== 'undefined') {
            if(checkboxupsell == false && checkboxgift == false){options = 0;}
            if(checkboxupsell && checkboxgift == false){options = 1;}
            if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg !== ''){options = 3;}
            if(checkboxupsell && checkboxgift && inputto === '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto === '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg !== ''){options = 2;}
            }; 
            if(typeof checkboxupsell === 'undefined') {
            if(checkboxgift && inputto === '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxgift && inputto === '' && inputfrom !== '' && inputmsg !== ''){options = 'default';}
            if(checkboxgift && inputto === '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxgift && inputto !== '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxgift && inputto === '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg !== ''){options = 2;}
            }
    switch(options) {
        case 0:
          // both without upsell and gift form
          SendtoCart(0);
          break;
        case 1:
          // with upsell without gift form
          event.preventDefault();
          SendtoCart(1);
          break;
        case 2:
          // without upsell but with gift form
          event.preventDefault();
          SendtoCart(2);
          break;
        case 3:
          // both with upsell && gift form
          event.preventDefault();
          SendtoCart(3);
          break;
        default:
          // no upsell but gift form required fields are empty
          if(inputto === '') {
            event.preventDefault();
          $('.product_gift_input_to').focus();
            return false;
          }
          if(inputfrom === '') {
            event.preventDefault();
            $('.product_gift_input_from').focus();
            return false;
          }
          if(inputmsg === '') {
            event.preventDefault();
            $('.product_gift_input_msg').focus();
            return false;
          }
          return true;
      }
function SendtoCart(option) {
    let formData;
    //option 0
    if(option == 0) {       
        $(this).click();
        return true;
    }  
    //option 1
    if(option == 1) {
        formData = {
            'items': [  
                        {
                        'id': upsellid,
                        'quantity': 1,
                        'properties': {
                            'Upsell Item': 'Accepted',
                            'Upsell from Variant ID': variantid,
                            'Batch ID': Date.now() / 1000 | 0
                            }
                        },
                        {
                        'id': variantid,
                        'quantity': variantqty,
                        'properties': {
                            'Batch ID': Date.now() / 1000 | 0
                            }
                        }
                 ]
           };
           $.ajax({
            type: 'POST',
            url: '/cart/add.js',
            data: formData,
            dataType: 'json',
            success: function() {
                window.theme.cart._promiseChange({
                    url: "/cart.json",
                    dataType: "json"
                });
                CheckCartItem();
                return true;  
            }
            }); 
    }
    //option 2
    if(option == 2) {
        formData = {
            'items': [  
                        {
                        'id': variantid,
                        'quantity': variantqty,
                        'properties': {
                            'Gift': 'Yes',
                            'To': inputto,
                            'From': inputfrom,
                            'Messages': inputmsg
                            }
                        }
                 ]
           };
           $.ajax({
            type: 'POST',
            url: '/cart/add.js',
            data: formData,
            dataType: 'json',
            success: function() {
                window.theme.cart._promiseChange({
                    url: "/cart.json",
                    dataType: "json"
                });
                CheckCartItem();
                window.location.href = '/checkout';
                return false;  
            }
            }); 
    }
    //option 3
    if(option == 3) {
        formData = {
            'items': [  
                        {
                        'id': upsellid,
                        'quantity': 1,
                        'properties': {
                            'Upsell Item': 'Accepted',
                            'Upsell from Variant ID': variantid,
                            'Batch ID': Date.now() / 1000 | 0
                            }
                        },
                        {
                        'id': variantid,
                        'quantity': variantqty,
                        'properties': {
                            'Gift': 'Yes',
                            'To': inputto,
                            'From': inputfrom,
                            'Messages': inputmsg,
                            'Batch ID': Date.now() / 1000 | 0
                            }
                        }
                 ]
           };
           $.ajax({
            type: 'POST',
            url: '/cart/add.js',
            data: formData,
            dataType: 'json',
            success: function() {
                window.theme.cart._promiseChange({
                    url: "/cart.json",
                    dataType: "json"
                });
                CheckCartItem();
                window.location.href = '/checkout';
                return false;  
            }
            }); 
        }
    }    
});
    $(d).on('click', 'button.shopify-payment-button__button', function(event) {
        var checkboxupsell = $('.product_upsell_variant').prop('checked'),
        checkboxgift = $('.product_gift_checkbox').prop('checked'),
        inputto = $('.product_gift_input_to').val(),
        inputfrom = $('.product_gift_input_from').val(),
        inputmsg = $('.product_gift_input_msg').val(),
        upsellid = $('.product_upsell_variant').val(),
        variantid = $('.product-form__master-select').val(),
        variantqty = $('[name="quantity"').val(),
        options = 0;
        if(typeof checkboxupsell !== 'undefined') {
            if(checkboxupsell == false && checkboxgift == false){options = 0;}
            if(checkboxupsell && checkboxgift == false){options = 1;}
            if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg !== ''){options = 3;}
            if(checkboxupsell && checkboxgift && inputto === '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell && checkboxgift && inputto === '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto === '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxupsell == false && checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg !== ''){options = 2;}
            }; 
            if(typeof checkboxupsell === 'undefined') {
            if(checkboxgift && inputto === '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxgift && inputto !== '' && inputfrom === '' && inputmsg === ''){options = 'default';}
            if(checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxgift && inputto === '' && inputfrom !== '' && inputmsg !== ''){options = 'default';}
            if(checkboxgift && inputto === '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxgift && inputto !== '' && inputfrom === '' && inputmsg !== ''){options = 'default';}
            if(checkboxgift && inputto === '' && inputfrom !== '' && inputmsg === ''){options = 'default';}
            if(checkboxgift && inputto !== '' && inputfrom !== '' && inputmsg !== ''){options = 2;}
            }
    switch(options) {
        case 0:
          // both without upsell and gift form
          SendtoCart(0);
          break;
        case 1:
          // with upsell without gift form
          event.preventDefault();
          SendtoCart(1);
          break;
        case 2:
          // without upsell but with gift form
          event.preventDefault();
          SendtoCart(2);
          break;
        case 3:
          // both with upsell && gift form
          event.preventDefault();
          SendtoCart(3);
          break;
        default:
          // no upsell but gift form required fields are empty
          event.preventDefault();
          if(inputto === '') {
            event.preventDefault();
          $('.product_gift_input_to').focus();
            return false;
          }
          if(inputfrom === '') {
            event.preventDefault();
            $('.product_gift_input_from').focus();
            return false;
          }
          if(inputmsg === '') {
            event.preventDefault();
            $('.product_gift_input_msg').focus();
            return false;
          }
  }
function SendtoCart(option) {
    let formData;
    //option 0
    if(option == 0) {      
        $(this).click();
        return true;
    }  
    //option 1
    if(option == 1) {
        formData = {
            'items': [  
                        {
                        'id': upsellid,
                        'quantity': 1,
                        'properties': {
                            'Upsell Item': 'Accepted',
                            'Upsell from Variant ID': variantid,
                            'Batch ID': Date.now() / 1000 | 0
                            }
                        },
                        {
                        'id': variantid,
                        'quantity': variantqty,
                        'properties': {
                            'Batch ID': Date.now() / 1000 | 0
                            }
                        }
                 ]
           };
           $.ajax({
            type: 'POST',
            url: '/cart/add.js',
            data: formData,
            dataType: 'json',
            success: function() {
                window.theme.cart._promiseChange({
                    url: "/cart.json",
                    dataType: "json"
                });
                CheckCartItem();
                window.location.href = '/checkout';
                return false;  
            }
            }); 
    }
    //option 2
    if(option == 2) {
        formData = {
            'items': [  
                        {
                        'id': variantid,
                        'quantity': variantqty,
                        'properties': {
                            'Gift': 'Yes',
                            'To': inputto,
                            'From': inputfrom,
                            'Messages': inputmsg
                            }
                        }
                 ]
           };
           $.ajax({
            type: 'POST',
            url: '/cart/add.js',
            data: formData,
            dataType: 'json',
            success: function() {
                window.theme.cart._promiseChange({
                    url: "/cart.json",
                    dataType: "json"
                });
                CheckCartItem();
                window.location.href = '/checkout';
                return false;  
            }
            }); 
    }
    //option 3
    if(option == 3) {
        formData = {
            'items': [  
                        {
                        'id': upsellid,
                        'quantity': 1,
                        'properties': {
                            'Upsell Item': 'Accepted',
                            'Upsell from Variant ID': variantid,
                            'Batch ID': Date.now() / 1000 | 0
                            }
                        },
                        {
                        'id': variantid,
                        'quantity': variantqty,
                        'properties': {
                            'Gift': 'Yes',
                            'To': inputto,
                            'From': inputfrom,
                            'Messages': inputmsg,
                            'Batch ID': Date.now() / 1000 | 0
                            }
                        }
                 ]
           };
           $.ajax({
            type: 'POST',
            url: '/cart/add.js',
            data: formData,
            dataType: 'json',
            success: function() {
                window.theme.cart._promiseChange({
                    url: "/cart.json",
                    dataType: "json"
                });
                CheckCartItem();
                window.location.href = '/checkout';
                return false;  
            }
            }); 
        }
    }    
});
function CheckCartItem(){
var keepcartchecking = setInterval(checking, 300);
function checking() {    
    window.theme.cart._promiseChange({
        url: "/cart.json",
        dataType: "json",
        success: function(data) {
            if(data.item_count>0) {
                ClearDynamicCheckoutBtn();
                stopchecking();
            } else {
                return true;
            }
        }
    });
}
function stopchecking() {
  clearInterval(keepcartchecking);
}
};
$(d).on("click", ".btn_contact_upload", function() {
        const client = filestack.init('ADuypK58CQeexcGTB0opWz');
        const logo_url = d.querySelector('[name="contact[logo_uploaded_url]"]');
        const options = {
            accept: ["image/*"],
            uploadConfig: {
                retry: 5,
                timeout: 60000
            },
            onFileSelected: file => {
                // If you throw any error in this function it will reject the file selection.
                // The error message will be displayed to the user as an alert.
                if (file.size > 2000 * 2000) {
                    throw new Error('File too big, select something smaller than 2MB');
                }
            },
            uploadInBackground: false,
            onFileUploadFinished: (response) => {
                logo_url.value = response.url;
                this.textContent = "Attached: " + response.filename;
            }
          };
          const picker = client.picker(options);
          picker.open();
    });
    $(d).on("click", ".product-form__plus", function() {
        var num = +$('[name="quantity"]').val() + 1;
        $('[name="quantity"]').val(num);
    });
    $(d).on("click", ".product-form__minus", function() {
        if($('[name="quantity"]').val()>1) {
        var num = +$('[name="quantity"]').val() - 1;
        $('[name="quantity"]').val(num);
        } else {
            return false;
        }
    });
    $(d).on("click", ".form-price-expand", function() {
        var form_price_quotation = d.querySelector(".form-price-quotation");
        this.classList.toggle("open");
        form_price_quotation.classList.toggle("height-opacity-zero");
    });
    $(d).on("click", ".form-price-title", function() {
        var form_price_expand = d.querySelector(".form-price-expand");
        var form_price_quotation = d.querySelector(".form-price-quotation");
        form_price_expand.classList.toggle("open");
        form_price_quotation.classList.toggle("height-opacity-zero");
    });
    $(d).on("mouseover", ".form-price-title", function() {
        var form_price_expand = d.querySelector(".form-price-expand");
        form_price_expand.classList.toggle("open");
    });
    $(d).on("mouseout", ".form-price-title", function() {
        var form_price_expand = d.querySelector(".form-price-expand");
        form_price_expand.classList.toggle("open");
    });
    $(d).on('click', '.product_gift_form', function() {
        if($('.product_gift_checkbox').prop("checked") == false) {
        $('.product_gift_checkbox').prop('checked', true);
        $('.product_gift_input_form').removeClass('d-none');
        $('.product_gift_input_to').attr('required','');
        $('.product_gift_input_from').attr('required','');
        $('.product_gift_input_msg').attr('required','');
        } else {
        $('.product_gift_checkbox').prop('checked', false);
        $('.product_gift_input_form').addClass('d-none');
        $('.product_gift_input_to').removeAttr('required','');
        $('.product_gift_input_from').removeAttr('required','');
        $('.product_gift_input_msg').removeAttr('required','');
        $('.product_gift_input_to').val('');
        $('.product_gift_input_from').val('');
        $('.product_gift_input_msg').val('');
        }
    });
    $(d).on('change', '.product_gift_checkbox', function() {
        if(this.checked) {
            $('.product_gift_checkbox').prop('checked', false);
            $('.product_gift_input_to').removeAttr('required','');
            $('.product_gift_input_from').removeAttr('required','');
            $('.product_gift_input_msg').removeAttr('required','');
            $('.product_gift_input_form').addClass('d-none');
            $('.product_gift_input_to').val('');
            $('.product_gift_input_from').val('');
            $('.product_gift_input_msg').val('');
            $(this).removeAttr('name');
            $(this).val('no');
        } else {
            $('.product_gift_checkbox').prop('checked', true);
            $('.product_gift_input_form').removeClass('d-none');    
            $('.product_gift_input_to').attr('required','');
            $('.product_gift_input_from').attr('required','');
            $('.product_gift_input_msg').attr('required','');
            $(this).attr('name','properties[gift]');
            $(this).val('yes');
        }
    });
    $(d).on('change', '.product_gift_checkbox', function() {
        if($('[data-shopify="payment-button"]')) {
        AddDynamicCheckoutBtn(this,'.product_upsell_variant');
        RemoveDynamicCheckoutBtn(this,'.product_upsell_variant');
        }
    });
    function AddDynamicCheckoutBtn(me,you) {
        if(me.checked && $(you).prop('checked') == false && $('[data-shopify="payment-button"]')) { 
            setTimeout(function() {   
            var base = d.querySelector('[data-shopify="payment-button"]');
            if(base) {
            var btn_xpress = base.cloneNode(true);   
            var btn_xsubmit = btn_xpress.children[0].children[0].children[0].children[1]; 
            var btn_xsubmit_other = btn_xpress.children[0].children[0].children[0].children[2]; 
            var parent = d.querySelector('[class="product-form"]');    
                btn_xsubmit.className = '';
                btn_xsubmit.classList.add('shopify-payment-button__button');
                btn_xsubmit.classList.add('shopify-payment-button__button--unbranded');
                btn_xsubmit.setAttribute('data-testid','ButtonXpress');
                btn_xsubmit.setAttribute('type','button');
                btn_xsubmit_other.className = '';
                btn_xsubmit_other.classList.add('shopify-payment-button__more-options');
                btn_xsubmit_other.classList.add('shopify-payment-button__button--hidden');
                btn_xsubmit_other.setAttribute('data-testid','ButtonXpressOther');
                btn_xsubmit_other.setAttribute('type','button');
                parent.insertBefore(btn_xpress,base);
                btn_xpress.setAttribute('data-id','dynamicbtnclone');
                base.setAttribute('data-id','dynamicbtnorig');
                base.classList.add('d-none');
            }
            },300);
            }  
            if(me.checked && typeof $(you).prop('checked') === 'undefined' && $('[data-shopify="payment-button"]')) { 
                setTimeout(function() {   
                var base = d.querySelector('[data-shopify="payment-button"]');
                if(base) {
                var btn_xpress = base.cloneNode(true);   
                var btn_xsubmit = btn_xpress.children[0].children[0].children[0].children[1]; 
                var btn_xsubmit_other = btn_xpress.children[0].children[0].children[0].children[2]; 
                var parent = d.querySelector('[class="product-form"]');    
                    btn_xsubmit.className = '';
                    btn_xsubmit.classList.add('shopify-payment-button__button');
                    btn_xsubmit.classList.add('shopify-payment-button__button--unbranded');
                    btn_xsubmit.setAttribute('data-testid','ButtonXpress');
                    btn_xsubmit.setAttribute('type','button');
                    btn_xsubmit_other.className = '';
                    btn_xsubmit_other.classList.add('shopify-payment-button__more-options');
                    btn_xsubmit_other.classList.add('shopify-payment-button__button--hidden');
                    btn_xsubmit_other.setAttribute('data-testid','ButtonXpressOther');
                    btn_xsubmit_other.setAttribute('type','button');
                    parent.insertBefore(btn_xpress,base);
                    btn_xpress.setAttribute('data-id','dynamicbtnclone');
                    base.setAttribute('data-id','dynamicbtnorig');
                    base.classList.add('d-none');
                }
                },300);
                }        
    };
    function RemoveDynamicCheckoutBtn(me,you) {
        var shopify_payment_button = d.querySelectorAll('.shopify-payment-button'); 
        if(me.checked == false && $(you).prop('checked') == false && shopify_payment_button.length>0) {   
            setTimeout(function() {   
                if($('[data-id="dynamicbtnorig"]')) {
                    $('[data-id="dynamicbtnorig"]').removeClass('d-none');
                    $('[data-id="dynamicbtnorig"]').removeAttr('data-id');
                };
                if($('[data-id="dynamicbtnclone"]')) { 
                    $('[data-id="dynamicbtnclone"]').remove()
                };
            },300);
        }
        if(me.checked == false && typeof $(you).prop('checked') === 'undefined' && shopify_payment_button.length>0) {   
            setTimeout(function() {   
                if($('[data-id="dynamicbtnorig"]')) {
                    $('[data-id="dynamicbtnorig"]').removeClass('d-none');
                    $('[data-id="dynamicbtnorig"]').removeAttr('data-id');
                };
                if($('[data-id="dynamicbtnclone"]')) { 
                    $('[data-id="dynamicbtnclone"]').remove()
                };
            },300);
        }
    }
    function ClearDynamicCheckoutBtn() {  
        if($('.product_upsell_variant').prop('checked')) {     
           $('.product_upsell_variant').click();
        }
        if($('.product_gift_checkbox').prop('checked')) {    
           $('.product_gift_checkbox').click();
        }
        $('.product_gift_input_form').addClass('d-none');
        setTimeout(function() {   
            if($('[data-id="dynamicbtnorig"]')) {
                $('[data-id="dynamicbtnorig"]').removeClass('d-none');
                $('[data-id="dynamicbtnorig"]').removeAttr('data-id');
            };
            if($('[data-id="dynamicbtnclone"]')) { 
                $('[data-id="dynamicbtnclone"]').remove()
            };
        },300);
    };
    $(d).on('change', '.product_upsell_variant', function() {
        if('[data-shopify="payment-button"]') {
        AddDynamicCheckoutBtn(this,'.product_gift_checkbox');
        RemoveDynamicCheckoutBtn(this,'.product_gift_checkbox');
        }
    });
    $(d).ready(function() {
     var navigation__entrance_animation = d.querySelectorAll('#shopify-section-header .navigation__entrance-animation');
        if($('.product_gift_checkbox').prop('checked')) {
            $('.product_gift_input_form').removeClass('d-none');
        }; 
        [].forEach.call(navigation__entrance_animation, function(el) {
            el.style.setProperty('opacity',1);
        });
     });
    $(d).on('click','[data-navigation-button]', function() {
    var nav_menu = d.querySelector('.site-header__section--button .navigation');
    var banner = d.querySelector('[swiper-container]');
    var burger_icon_mid = d.querySelector('.burger-icon--mid');    
        if(nav_menu){nav_menu.classList.toggle('mobile-d-none')};
        if(burger_icon_mid){burger_icon_mid.classList.toggle('d-none')};
        if(banner){banner.classList.toggle('d-none')};
       
            if(this.getAttribute('aria-expanded') === 'true') {
                var locale_selectors__selector = d.querySelector('.locale-selectors__container form');
                if(locale_selectors__selector){locale_selectors__selector.classList.remove('d-none');}
            } else {
                var locale_selectors__selector = d.querySelector('.locale-selectors__container form');
                if(locale_selectors__selector) { 
                    locale_selectors__selector.classList.add('d-none');
                } else {
                    let myselector = setInterval(keeplooking, 200);
                            function keeplooking() {
                                var locale_selectors__selector = d.querySelector('.locale-selectors__container form');
                                if(locale_selectors__selector) {
                                    locale_selectors__selector.classList.add('d-none');
                                    clearInterval(myselector);                                    
                                } 
                             }
            }
        }
    });
    $(d).on('click', '.ajax-cart__toggle', function() {
    var cart_drawer = d.querySelector('[data-section-id="cart-drawer"]');
        if(cart_drawer.classList.contains('navigation-hide')) {
            cart_drawer.classList.remove('navigation-hide');
        } 
    });
    $(d).on('click', '.navigation__expand-sublinks', function() {
        var navsublink = d.querySelector('.navigation__has-sublinks');
        var sublinks_container = d.querySelector('.navigation__sublinks-container');
        if(sublinks_container.classList.contains('sublinks-container-max-height-zero')) {
           sublinks_container.classList.add('sublinks-container-max-height-hundred');
           sublinks_container.classList.remove('sublinks-container-max-height-zero');
           navsublink.classList.remove('navigation-max-height-none');
        } else {
            sublinks_container.classList.remove('sublinks-container-max-height-hundred');
            sublinks_container.classList.add('sublinks-container-max-height-zero');
            navsublink.classList.add('navigation-max-height-none');
        }
    });
    })($,window,document);